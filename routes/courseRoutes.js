const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth");

// Route for creating a course
router.post("/create", auth.verify, (req, res) => {

  // Method #1
  // contains the data needed for creating a course
  const data = {
    course: req.body, //name, description, price
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  if(data.isAdmin) {

    courseController.addCourse(data.course).then(resultFromController => res.send (resultFromController));

  } else {

    res.send(false);

  }

  


  /*

    Method #2

      const userData = auth.decode(req.headers.authorization);
      courseController.addCourse(req.body).then(resultFromController => res.send (resultFromController));

  */
});

module.exports = router;
